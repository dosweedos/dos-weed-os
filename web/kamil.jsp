<%-- 
    Document   : kamil
    Created on : Nov 14, 2015, 12:44:53 AM
    Author     : Asad
--%>

<%@page import="java.io.PrintWriter"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       
        
        <title>HTML</title>
         <%
             
        String c = request.getParameter("color");
        String t=request.getParameter("text");
        String tc=request.getParameter("tcolor");
        String tf=request.getParameter("font");
         %>
        <% 
        if(c.equals("red")){%>
        <style type="text/css">
            body
            {
                background-color: red;
            }
            
        </style>
        <%}%>
        <%
        if(c.equals("white")){%>
        <style type="text/css">
            body{
                background-color: white;
            }
            
        </style>
        <%}%>
        <%
        if(c.equals("blue")){%>
        <style type="text/css">
            body{
                background-color: blue;
            }
            
        </style>
        <%}%>
        <% 
        if(c.equals("green")){%>
        <style type="text/css">
            body{
                background-color: green;
            }
        </style>
        <%}%>
        <% 
        if(tc.equals("gray")){%>
        <style type="text/css">
            h1
            {
                color: gray;
            }
            
        </style>
        <%}%>
        <% 
        if(tc.equals("black")){%>
        <style type="text/css">
            h1
            {
                color: black;
            }
            
        </style>
        <%}%>
        <% 
        if(tc.equals("orange")){%>
        <style type="text/css">
            h1
            {
                color: orange;
            }
            
        </style>
        <%}%>
        <% 
        if(tc.equals("violet")){%>
        <style type="text/css">
            h1
            {
               color: violet;
            }
            
        </style>
        <%}%>
        <% 
        if(tf.equals("sans-serif")){%>
        <style type="text/css">
            h1
            {
               font-family: sans-serif;
            }
            
        </style>
        <%}%>
           <% 
        if(tf.equals("serif")){%>
        <style type="text/css">
            h1
            {
               font-family: serif;
            }
            
        </style>
        <%}%>
           <% 
        if(tf.equals("times new roman")){%>
        <style type="text/css">
            h1
            {
               font-family: Times New Roman;
            }
            
        </style>
        <%}%>
           <% 
        if(tf.equals("impact")){%>
        <style type="text/css">
            h1
            {
               font-family: Impact;
            }
            
        </style>
        <%}%>
    </head>
    <body>
        <form action="infoservlet" method="POST">
            Page's background-color: <select name="color">
                <option value="white">White</option>
                <option value="green">Green</option>
                <option value="red">Red</option>
                <option value="blue">Blue</option>
            </select>
            <br>
            Type your favorite quote and share with us: <input type="text" name="text" class="text">
            <br />
            Choose color for you quote: <select name="tcolor" >
                <option value="gray">Gray</option>
                <option value="black">Black</option>
                <option value="orange">Orange</option>
                <option value="violet">Violet</option>
            </select>
            <br />
            <br />
            Choose font-family for your quote:
            <select name="font">
                <option value="sans-serif">Sans-Serif</option>
                <option value="serif">Serif</option>
                <option value="times new roman">Times New Roman</option>
                <option value="impact">Impact</option>
            </select>
            <br />
            <input type="submit">
        </form>
         <p>
            Enjoy this page. Depending on your taste and style choose appropriate color, image for your page...
        </p>
        <% response.setContentType("text/html;charset=UTF-8");
            out.println("<h1>"+t+"</h1>");
        %>
       </body>
        
</html>
